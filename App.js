import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    PermissionsAndroid, Dimensions,
    Text,
} from 'react-native';

import {
    ViroARSceneNavigator
} from 'react-viro';

import Config from 'react-native-config'
import RNSimpleCompass from "react-native-simple-compass";

var sharedProps = {
//    apiKey: Config.REACT_APP_VIRO_API_KEY
};

var foursquare = require('react-foursquare')({
    clientID: Config.REACT_APP_FOURSQUARE_CLIENT_ID,
    clientSecret: Config.REACT_APP_FOURSQUARE_CLIENT_SECRET
});

// Sets the default scene you want for AR and VR
var InitialARScene = require('./js/EnviromentScene');

export const requestPermissions = async () => {
    try {
        let camera, gps;
        do {
            gps = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            )
            camera = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA
            )
        } while (gps !== PermissionsAndroid.RESULTS.GRANTED && camera !== PermissionsAndroid.RESULTS.GRANTED);

        return true;
    } catch (err) {
        console.warn(err)
    }
};


/*export const _getLocation = async () => {
    navigator.geolocation.getCurrentPosition(position => { //change to position if degree doesnt work
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                altitude: position.coords.altitude
            });
        }, error => console.warn(error.message),
        {enableHighAccuracy: true, timeout: 10000, maximumAge: 10000}
    );
};*/

export default class Virtualenv extends Component {

    constructor() {
        super();

        this._getHeading = this._getHeading.bind(this);
        this._getLocation = this._getLocation.bind(this);
        this._getFoursquareVenues = this._getFoursquareVenues.bind(this);

        this.state = {
            sharedProps: sharedProps,
            viroAppProps: {_getHeading: this._getHeading, _getLocation: this._getLocation, location: null, heading: null},
        };
    }

    async componentDidMount() {
        await requestPermissions();
        this._getHeading();
        let result = await this._getLocation();
        this.setState({'viroAppProps': { ...this.state.viroAppProps, location: result} });
        await this._getFoursquareVenues();
    }

    // Replace this function with the contents of _getVRNavigator() or _getARNavigator()
    // if you are building a specific type of experience.
    render() {
        if(this.state.viroAppProps.location === null || !this.state.viroAppProps.venueList){
            return (
                <View style={styles.inner}>
                    <Text>Inicializando</Text>
                    <Text>{this.state.viroAppProps.location === null ? "No hay localización" : "Localización: "+this.state.viroAppProps.location['latitude']+" "+this.state.viroAppProps.location['longitude']}</Text>
                    <Text>{this.state.viroAppProps.venueList === undefined ? "No se ha recibido com. con la api" : "Sitios: "+ this.state.viroAppProps.venueList.length}</Text>
                </View>
            )
        }else {
            return (
                <View style={styles.outer}>
                    <ViroARSceneNavigator {...this.state.sharedProps} autofocus={false}
                                          worldAlignment={'GravityAndHeading'}
                                          videoQuality={'High'}
                                          shadowsEnabled={false}
                                          pbrEnabled={false}
                                          hdrEnabled={false}
                                          bloomEnabled={false}

                                          initialScene={{scene: InitialARScene}}
                                          viroAppProps={this.state.viroAppProps}/>
                    <View style={styles.crosshair}/>
                </View>
            )
        }
    }

    async _getLocation() {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                position => resolve(position.coords),
                error => reject(error),
                {enableHighAccuracy: true, timeout: 20000, maximumAge: 0}
            )
        });
    };

    _getHeading() {
        // Get heading direction
        RNSimpleCompass.start(1, (degree) => {
            //this.state.heading = degree;
            //this.setState({'heading': degree});
            this.setState({'viroAppProps': { ...this.state.viroAppProps, heading: degree} });
            RNSimpleCompass.stop();
        });
    }

    async _getFoursquareVenues() {
        let params = {"ll": this.state.viroAppProps.location['latitude'] + "," + this.state.viroAppProps.location['longitude']};
        let response = foursquare.venues.getVenues(params)
            .then(res => {
                this.setState({'viroAppProps': { ...this.state.viroAppProps, venueList: res.response.venues}});
            });
    }
}

var styles = StyleSheet.create({
    viroContainer: {
        flex: 1,
        backgroundColor: "black",
    },
    outer: {
        flex: 1,
        flexDirection: 'row',
    },
    inner: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: "white",
    },
    titleText: {
        paddingTop: 30,
        paddingBottom: 20,
        color: '#fff',
        textAlign: 'center',
        fontSize: 25
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20
    },
    buttons: {
        height: 80,
        width: 150,
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#68a0cf',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    exitButton: {
        height: 50,
        width: 100,
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#68a0cf',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    crosshair: {
        position: 'absolute',
        top: (Dimensions.get('window').height / 2),
        left: (Dimensions.get('window').width / 2),
        width: 10,
        height: 10,
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor: 'grey',
    },
});

module.exports = Virtualenv
