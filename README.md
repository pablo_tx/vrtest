# AR Base

Project description here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Install the following packages
```
android-tools
android-sdk-platform-tools
android-sdk-build-tools
android-platform version 23
```

Run adb server
```
sudo adb start-server
```

Edit your ~/.bashrc or  ~/.zshrc
```
export ANDROID_HOME=/opt/android-sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
```

Accept all licenses
```
sudo sdkmanager --licenses
```

Install openjdk 8
```
openjdk-8-jdk
```

To run sdk as regular user we need to create Android sdk users group and add your user to that group:
```
groupadd sdkusers
gpasswd -a <user> sdkusers
chown -R g+w $pathToSDK
chmod -R g+w $pathToSDK
newgrp sdkusers
```


### Installing

```
react-native run-android --variant=gvrDebug
```


## Built With

* [VIRO React](https://viromedia.com/viroreact)

## Authors

* **Pablo Pozo Tena** - *Desarrollo* - [pablo-tx](https://github.com/pablo-tx)

* **Victor Moreno Jimenez** - *Desarrollo* - [VictorMorenoJimenez](https://github.com/VictorMorenoJimenez)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details