'use strict';

/**
 * Pull in all imports required for the controls within this scene.
 */
import React, {Component} from 'react';
import {Platform, StyleSheet} from 'react-native';

import {
    ViroARScene,
    ViroAnimations,
    ViroNode,
    ViroText,
    ViroConstants,
    ViroImage,
} from 'react-viro';
import RNSimpleCompass from "react-native-simple-compass";
//import ThreeSixtyScene from "./ThreeSixtyScene";


//const degree_update_rate = 3; // Number of degrees changed before the callback is triggered

var InfoElement = require('./custom_controls/InfoElement');

var demoImage = require('./res/guadalupe_360.jpg');
var icon360 = require('./res/icon_360.png');

var ThreeSixtyScene = require('./ThreeSixtyScene');

export default class EnviromentScene extends Component {

    constructor() {
        super();

        // set initial state
        this.state = {
            pointList: [
                /*{id: 1, text: "Alhambra", latitude: 37.176189, longitude: -3.588161, ARPointX: 0, ARPointZ: 0},
                {id: 2, text: "Parque", latitude: 37.193723, longitude: -3.610583, ARPointX: 0, ARPointZ: 0},
                {id: 3, text: "Camara Comercio", latitude:  37.194646, longitude: -3.614419, ARPointX: 0, ARPointZ: 0}*/
            ],
            latitude: null,
            longitude: null,
            altitude: null,
            heading: null,
            isTracking: false,
            initialized: false,
            venueList: [],
        };

        // bind 'this' to functions
        this._onTrackingUpdated = this._onTrackingUpdated.bind(this);
        this._latLongToMerc = this._latLongToMerc.bind(this);
        this._transformPointToAR = this._transformPointToAR.bind(this);
        this._getLocation = this._getLocation.bind(this);
        this._getVenueList = this._getVenueList.bind(this);
        this._calculatePoints = this._calculatePoints.bind(this);
        this._drawScene = this._drawScene.bind(this);
        this._load360Scene = this._load360Scene.bind(this);
    }


    _drawScene() {
        if (this.state.isTracking) {
            return (
                <ViroNode>
                    <ViroText text={"Dir: " + this.state.heading + '\n' +
                    "Location: " + this.state.latitude + " " + this.state.longitude} position={[0, 1, -5]}/>

                    <ViroImage
                        position={[0, 1, -3]} source={icon360} scale={[1, 1, 1]} onClick={this._load360Scene}
                    />

                    {this.state.pointList.map((item, index) => <InfoElement key={index} name={item.text}
                                                                            transformBehaviors={["billboard"]}
                                                                            scale={Math.abs(item.ARPointZ) > 100 ? [20, 20, 20] : Math.abs(item.ARPointZ) > 50 ? [10, 10, 10] : [5, 5, 5]}
                                                                            content={demoImage}
                                                                            contentCardScale={[3.67, 4, 1]}
                                                                            position={[item.ARPointX, 0, item.ARPointZ]}/>)}

                </ViroNode>
            )
        } else {
            return (
                <ViroNode>
                    <ViroText text={"No tracking"} position={[0, 0, -2]}/>
                </ViroNode>
            )
        }
    }

    componentDidMount(): void {
        this._getLocation();
        this._getHeading();
        this._getVenueList();
    }

    /**
     *
     */
    render() {
        return (
            <ViroARScene onTrackingUpdated={this._onTrackingUpdated}>
                {this._drawScene()}
            </ViroARScene>
        );
    }

    _onTrackingUpdated = (state, reason) => {
        if (state === ViroConstants.TRACKING_NORMAL) {
            this._calculatePoints();
            this.setState({'isTracking': true})
        } if (state === ViroConstants.TRACKING_UNAVAILABLE) {
            this.setState({'isTracking': false})
        }
    };

    _calculatePoints() {
        this._getHeading();
        console.log("Current Location: " + this.state.latitude + ", " + this.state.longitude);
        console.log("Current Heading: " + this.state.heading);


        /*for (let i in this.state.pointList.length) {
            let point = this.state.pointList[i];
            let ARPoint = this._transformPointToAR(point.latitude, point.longitude);

            //console.log("ARPoint.x: " + ARPoint.x + " ARPoint.z: " + ARPoint.z);

            this.state.pointList[i].ARPointX = ARPoint.x;
            this.state.pointList[i].ARPointZ = ARPoint.z;
        }*/
        this.setState({'pointList': []});

        if(this.state.venueList) {
            for (let venue of this.state.venueList) {
                let latitude = venue['location']['lat'];
                let longitude = venue['location']['lng'];
                let name = venue['name'];
                let ARPoint = this._transformPointToAR(latitude, longitude);
                let newPoint = {
                    'id': this.state.pointList.length + 1,
                    'text': name,
                    'latitude': latitude,
                    'longitude': longitude,
                    'ARPointX': ARPoint.x,
                    'ARPointZ': ARPoint.z
                };

                this.setState(prevState => ({
                    pointList: [...prevState.pointList, newPoint]
                }))
            }
        }
    }

    _latLongToMerc(lat_deg, lon_deg) {
        let lon_rad = (lon_deg / 180.0 * Math.PI);
        let lat_rad = (lat_deg / 180.0 * Math.PI);
        let sm_a = 6378137.0;
        let xmeters = sm_a * lon_rad;
        let ymeters = sm_a * Math.log((Math.sin(lat_rad) + 1) / Math.cos(lat_rad));
        return ({x: xmeters, y: ymeters});
    }

    // TODO definitively this doesn't work properly
    _transformPointToAR(lat, long) {
        // latitude(north,south) maps to the z axis in AR
        // longitude(east, west) maps to the x axis in AR

        // LatLong To Mercator
        let objPoint = this._latLongToMerc(lat, long);
        //console.log("objPoint.x: " + objPoint.x + ", objPoint.y: " + objPoint.y);
        let devicePoint = this._latLongToMerc(this.state.latitude, this.state.longitude);
        //console.log("devicePointX: " + devicePoint.x + ", devicePointY: " + devicePoint.y);

        //Calculate position
        let objDeltaX = (objPoint.x - devicePoint.x);
        let objDeltaY = (objPoint.y - devicePoint.y);
        //console.log("objDeltaX: " + objDeltaX + ", objDeltaY: " + objDeltaY);

        if (Platform.OS === 'android') {
            // Heading to radians
            let heading_rad = (this.state.heading * Math.PI) / 180.0;

            // Rotate position with current heading direction
            let rotatedObjPointX = objDeltaX * Math.cos(heading_rad) - objDeltaY * Math.sin(heading_rad);
            let rotatedObjPointZ = objDeltaX * Math.sin(heading_rad) + objDeltaY * Math.cos(heading_rad);
            //console.log("rotatedObjPointX: " + rotatedObjPointX + ", rotatedObjPointZ: " + -rotatedObjPointZ);

            // Flip the z, as negative z(is in front of us which is north, pos z is behind(south).
            return ({x: rotatedObjPointX, z: -rotatedObjPointZ});
        }

        return ({x: objDeltaX, z: -objDeltaY});
    }

    _getLocation() {
        this.setState({'latitude': this.props.arSceneNavigator.viroAppProps.location['latitude'],
                            'longitude': this.props.arSceneNavigator.viroAppProps.location['longitude'],
                            'altitude': this.props.arSceneNavigator.viroAppProps.location['altitude']})
    };



    _getHeading() {
        if(this.state.heading === null) {
            this.setState({'heading': this.props.arSceneNavigator.viroAppProps.heading})
        }else {
            RNSimpleCompass.start(3, (degree) => {
                //this.state.heading = degree;
                this.setState({'heading': degree});
                RNSimpleCompass.stop();
            });
        }
    }

    _getVenueList() {
        this.setState({'venueList': this.props.arSceneNavigator.viroAppProps.venueList});
    }

    _load360Scene(){
        this.props.sceneNavigator.push({scene:ThreeSixtyScene});
    }
}

/**
 * Declare all custom flex box styles here to be reference by the
 * controls above.
 */
var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        fontFamily: 'Arial',
        fontSize: 30,
        color: '#FFFFFF',
        textAlignVertical: 'center',
        textAlign: 'center',
    },
    nearPoint: {
        fontFamily: 'Arial',
        fontSize: 10,
        color: '#FFFFFF',
    },
    mediumPoint: {
        fontFamily: 'Arial',
        fontSize: 20,
        color: '#FFFFFF',
    },
    farPoint: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontFamily: 'Arial',
        fontSize: 300,
        color: 'red',
    },
});

/**
 * Declare all your animations here. They'll be referenced by the animation props.
 */
ViroAnimations.registerAnimations({
    showTitleAnimation: {properties: {scaleX: 2, scaleY: 2, scaleZ: 2, opacity: 1.0}, duration: 1000},
});

module.exports = EnviromentScene;
